#!/bin/bash
# I use this script to build and publish deb packages in ppa:mikhailnov/utils (https://launchpad.net/~mikhailnov/+archive/ubuntu/utils)
# I publish it to allow other people to use it and make it possible to maintain a new PPA easily in case I stop doing it for some reason
# I think, it can also be used for maintaining packages in mainline Debian (minor modifications required)

build_package(){
	pkg_name="$(echo $directory | awk -F "-" '{print $1}')"

	debian/rules clean
	dir0="$(pwd)"

	for i in eoan focal
	do
		old_header=$(head -1 ./debian/changelog)
		old_version="$(cat ./debian/changelog | head -n 1 | awk -F "(" '{print $2}' | awk -F ")" '{print $1}')"
		new_version="${old_version}~${i}1"
		sed -i -re "s/${old_version}/${new_version}/g" ./debian/changelog
		sed -i -re "1s/unstable/$i/" ./debian/changelog
		# -I to exclude .git; -d to allow building .changes file without build dependencies installed
		dpkg-buildpackage -I -S -sa -d
		sed  -i -re "1s/.*/${old_header}/" ./debian/changelog
		cd ..
		
		for ppa_name in ppa:mikhailnov/utils
		do
			# example file name: pulseeffects_4.4.3-1~bionic1_source.changes
			dput -f "$ppa_name" "$(ls -tr ${pkg_name}_*_source.changes | tail -n 1)"
		done
		
		cd "$dir0"
		sleep 1
	done

	debian/rules clean
	#cd "$dir_start"
}

dir_start="$(pwd)"
for directory in "tor-0.4.2.6"
do
	cd "$directory"
	build_package #"$directory"
	cd "$dir_start"
done
